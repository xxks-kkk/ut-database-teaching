CREATE USER utdb WITH PASSWORD 'passw0rd' REPLICATION;

CREATE DATABASE ee382v OWNER utdb;
\c ee382v

BEGIN;

DROP TABLE IF EXISTS R,S;

CREATE TABLE R (
       name varchar(10),
       serviceId integer
);

CREATE TABLE S (
       serviceId integer,
       service varchar(10)
);

INSERT INTO R(name,serviceId)
VALUES
('zeyuan', null),
('Lokendra', null),
('tiratat', 11),
('Dan', 9),
('jatin', 3),
('anu', 12),
('matt', 9);

INSERT INTO S(serviceId,service)
VALUES
(10, 'EC2'),
(9, 'Timestream'),
(11, 'Lambda'),
(12, 'S3'),
(3, 'DocumentDB'),
(4, 'Aurora');

COMMIT;
