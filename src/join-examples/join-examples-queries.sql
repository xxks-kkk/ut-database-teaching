\c ee382v
-- Query 1 --
select name from R,S where R.serviceid = S.serviceid;

-- Query 2 --
select name from R join S on r.serviceid = s.serviceid;

-- Query 3 --
select R.name from R where exists (select S.service from S where R.serviceid = S.serviceid);

-- Query 4 --
select R.name from R where R.serviceid in (select serviceid from S);
